module PrincipalsHandler

  #uses if not configured ENV variables for remote API server (see application.rb)
  def load_from_file
    puts "--------------loaded from file---------------"
    json = ActiveSupport::JSON.decode(File.read('db/data.json'))
    principals = []
    json['principals'].each do |principal_hash|
      principals << Principal.new(principal_hash)
    end if json['principals']
    principals
  end

  def load_from_remote
    puts "--------------loaded from remote api server---------------"

    conn = Faraday.new(:url => ENV['api_base_url'].to_s) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end

    responce = conn.get do |req|
      req.url ENV['api_path']
      req.headers['Content-Type'] = 'application/json'
      req.headers['API-Key'] = "#{ENV['api_key']}"
    end

    if responce.status.eql?(200)
      ActiveSupport::JSON.decode(responce.body).map {|principal_hash| Principal.new(principal_hash) }
    else
      responce.status
    end

  end

end