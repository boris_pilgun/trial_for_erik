require 'spec_helper'

describe 'home/index' do
  before do
    json = File.read('spec/support/test_data.json')
    stub_request(:get, "http://testurl.com/api/v1/principals/list").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Api-Key'=>'goodkey', 'Content-Type'=>'application/json', 'User-Agent'=>'Faraday v0.8.7'}).
        to_return(:status => 200, :body => json, :headers => {})

    assign :principals, Principal.load_all
    assign :status, 200
  end

  it 'has correct content' do
    render.should have_content 'ID'
    render.should have_content 'NAME'
    render.should have_content 'SUPPLIER_GUID'
    render.should have_content 'Name 222'
    render.should have_content 'id333'
  end

end