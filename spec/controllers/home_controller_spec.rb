require 'spec_helper'

describe HomeController do
  before {
    json = File.read('spec/support/test_data.json')
    stub_request(:get, "http://testurl.com/api/v1/principals/list").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Api-Key'=>'goodkey', 'Content-Type'=>'application/json', 'User-Agent'=>'Faraday v0.8.7'}).
        to_return(:status => 200, :body => json, :headers => {})
  }

  describe "GET index" do
    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
      response.should be_success
    end
  end

end