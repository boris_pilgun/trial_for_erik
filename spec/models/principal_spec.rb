require 'spec_helper'
require 'webmock/rspec'

describe Principal do

  it 'should properly initialize object' do
    obj = Principal.new(:id => '123', :name => 'Name 123',  :supplier_guid => 'id123')
    obj.id.should eql '123'
    obj.name.should eql 'Name 123'
    obj.supplier_guid.should eql 'id123'
  end

  describe 'loading data' do

    it 'should load data from stubbed remote api server' do

      #test when Principal model should load from remote api service
      ENV['api_key'] = 'goodkey'
      ENV['api_base_url'] = 'http://testurl.com'
      ENV['api_path'] = '/api/v1/principals/list'

      json = File.read('spec/support/test_data.json')

      stub_request(:get, "http://testurl.com/api/v1/principals/list").
          with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Api-Key'=>'goodkey', 'Content-Type'=>'application/json', 'User-Agent'=>'Faraday v0.8.7'}).
          to_return(:status => 200, :body => json, :headers => {})

      @principals = Principal.load_all
      @principals.size.should eql 3
      @principals.each_with_index do |p, i|
        p.id.should eql ((i+1).to_s * 3).to_i
        p.name.should eql "Name #{(i+1).to_s * 3}"
        p.supplier_guid.should eql "id#{(i+1).to_s * 3}"
      end
    end


    it 'should load data from stubbed remote api server' do

      #test when Principal model should load from remote api service
      ENV['api_key'] = ''
      ENV['api_base_url'] = 'http://testurl.com'
      ENV['api_path'] = ''

      json = ActiveSupport::JSON.decode(File.read('db/data.json'))

      @principals = Principal.load_all
      @principals.size.should eql json['principals'].size
      @principals.each_with_index do |p, i|
        p.id.should eql json['principals'][i]['id']
        p.name.should eql json['principals'][i]['name']
        p.supplier_guid.should eql json['principals'][i]['supplier_guid']
      end
    end


  end

end