class HomeController < ApplicationController

  def index
    @principals = Principal.load_all
    @status = @principals.is_a?(Array) ? 200 : @principals
  end

end