class Principal

  extend PrincipalsHandler

  attr_accessor :id, :name, :supplier_guid

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def self.load_all
    if ENV['api_key'].blank? || ENV['api_base_url'].blank? || ENV['api_path'].blank?
      load_from_file
    else
      load_from_remote
    end
  end

end